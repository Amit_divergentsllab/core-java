package com.divergent.core_java;

/**
 * @author Amit kumar singh chauhan
 *
 */

public class Overloading {
	public static class Sum {
		  
	    // This sum takes two int parameters
	    public int sum(int x, int y)
	    {
	        return (x + y);
	    }
	  
	    //  This sum takes three int parameters
	    public int sum(int x, int y, int z)
	    {
	        return (x + y + z);
	    }
	  
	    //  This sum takes two double parameters
	    public double sum(double x, double y)
	    {
	        return (x + y);
	    }
	  
	    // Driver code
	    public static void main(String args[])
	    {
	        Sum s = new Sum();
	        System.out.println(s.sum(20, 30));
	        System.out.println(s.sum(20, 30, 40));
	        System.out.println(s.sum(20.5, 40.5));
	    }
	}
}

