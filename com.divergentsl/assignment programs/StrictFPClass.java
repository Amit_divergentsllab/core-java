package com.divergent.core_java;
strictfp class StrictFPClass {
    double num1 = 10e+102;
    double num2 = 6e+08;
    double calculate() {
        return num1 + num2;
    }
}