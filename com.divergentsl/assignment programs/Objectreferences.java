package com.divergent.core_java;

import java.io.*;

/**
 * @author Amit kumar singh chauhan
 *
 */
class Example {
    int x = 30;
    int display()
    {
        System.out.println("x = " + x);
        return 0;
    }
}
 
class Objectreferences {
    public static void main(String[] args)
    {
        Example D1 = new Example(); // point 1
 
        System.out.println(D1); // point 2
 
        System.out.println(D1.display()); // point 3
    }
}

 
