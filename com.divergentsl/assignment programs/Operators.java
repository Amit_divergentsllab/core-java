package com.divergent.core_java;
public class Operators{
	
	    public static void main(String[] args)
	    {
	 
	        int a = 5;
	        int b = -10;
	 
	        // left shift operator
	       
	        System.out.println("a<<2 = " + (a << 2));
	 
	        // right shift operator
	        
	        System.out.println("b>>2 = " + (b >> 2));
	 
	        // unsigned right shift operator
	        System.out.println("b>>>2 = " + (b >>> 2));
	    }
	}
