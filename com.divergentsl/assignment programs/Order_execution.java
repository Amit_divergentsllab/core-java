/**
 * 
 */
package com.divergent.core_java;
/**
 * @author Amit kumar singh chauhan
 *
 */

public class Order_execution {
		static class ABC {
			  
		    ABC(int x)
		    {
		        System.out.println("ONE argument");
		    }
		  
		    ABC()
		    {
		        System.out.println(" No argument");
		    }
		  
		    public static void main(String[] args)
		    {
		        new ABC();
		        new ABC();
		    }
		}

	}

