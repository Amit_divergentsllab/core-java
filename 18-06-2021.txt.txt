﻿Q-When we use strictfp?

Ans- strictfp is used to ensure that floating points operations give the same result on any platform. As floating points precision
may vary from one platform to another. strictfp keyword ensures the consistency across the platforms.

strictfp can be applied to class, method or on interfaces but cannot be applied to abstract methods, variable or on constructors.

Q-Write a program that uses strictfp.

Ans- strictfp class StrictFPClass {
    double num1 = 10e+102;
    double num2 = 6e+08;
    double calculate() {
        return num1 + num2;
    }
}

Q-In which case you will use strictfp,"Optimum performance" or "Perfect reproducibility"?

Ans- Optimum performance can be achieved when throughout 64 bit is not used(used 80-bit if 80 bit if CPU has 80-bit register) used but in that 
case the same operation on different machines gives different results
# Perfect reproducibility can be achieved by using the 64 bit throughout 
the calculations and doing truncation after each sub-operation to 64-bit.
# By default, virtual machine designers are now permitted to use 
extended precision for intermediate computations(i.e. Optimum 
performance)
# However, methods tagged with the strictfp keyword must use strict 
floating-point operations that yield reproducible results.



Q-In which call cases you cannot use "strictfp".

Ans- strictfp cannot be used with abstract methods. However, it can be used with abstract classes/interfaces.
     Since methods of an interface are implicitly abstract, strictfp cannot be used with any method inside an interface.

Q-What would be the values of a,b,m and n after execution of these two statements? 
       
1-int a=2 * ++m;//now a is 18,m is 8.

Ans-  public class Test{
      public static void main(string[] args){
        int m = 8;
        int a = 2 * ++m; // now a is 16, m is 8
        system.out.println(a);
  }
}

 2.
int b=2 *++n;//now b is 16,n is 8.

Ans-  public class Test{
      public statis void main(string[] args){
        int n = 8;
        int b = 2 * n++; // now b is 14, n is 8
        system.out.printin(b);
  }
}

Q-Write a program to use shift operators(>>,<<,>>>),use binary numbers(i.e.OB...)for the operations.

Ans-*example for ">>"
  class test {
         public static void main(string args[]) {
             int x = -4;
             system.out.printin(x>>1);
             int y = 4;
             system.out.printin(y>>1);
         }
}

output:
-2
2

* example for ">>>"
       class test {
         public static void main(string args[]) {
             // x is stored  using 32 bit 22's complement form.
             // binary representation of -1 is all 1s (111..1)
             int x = -1;
             system.out.printin(x>>>29);  // the value of 'x>>>29' is 00...0111
             system.out.printin(x>>>30);  // the value of 'x>>>30' is 00...0011
             system.out.printin(x>>>31);  // the value of 'x>>>31' is 00...0001

            }
   }

 output:
7
3
1
Q-Why the logical operators "&&" and "||" called "short circuit" operators.

Ans-  if the evaluation of a logical expression exit in between before complete evaluation, then it is known as Short-circuit. A short circuit happens because the result is clear even before the complete evaluation of the expression, and the result is returned. Short circuit evaluation avoids unnecessary work and leads to efficient processing.
 In case of AND, the expression is evaluated until we get one false result because the result will always be false, independent of the further conditions. If there is an expression with &&(logical AND), and first operand itself is false, then short circuit occurs, the further expression is not evaluated and false is returned.
In case of OR, the expression is evaluated until we get one true result because the result will always be true, independent of the further conditions. If there is an expression with ||(logical OR), and first operand itself is true, then a short circuit occurs, evaluation stops and true is returned.

Q-Write a program to prove that you can avoid NullPointerException using "&&" operator.

Ans-
import java.util.UUID;
import java.io.*;

class Singleton
{
	// Initializing values of single and ID to null.
	private static Singleton single = null;
	private String ID = null;

	private Singleton()
	{
		/* Make it private, in order to prevent the
		creation of new instances of the Singleton
		class. */

		// Create a random ID
		ID = UUID.randomUUID().toString();
	}

	public static Singleton getInstance()
	{
		if (single == null)
			single = new Singleton();
		return single;
	}

	public String getID()
	{
		return this.ID;
	}
}

// Driver Code
public class TestSingleton
{
	public static void main(String[] args)
	{
		Singleton s = Singleton.getInstance();
		System.out.println(s.getID());
	}
}

Q-Why should we use Math.multiplyExact and other xxExact methods?

Ans- The Java Math multiplyExact() method multiplies the specified numbers and returns it.Here, multiplyExact() is a static method. Hence, we are accessing the method using the class name, Math.

Q-Write a program that uses the various Math.xxxExact methods?

Ans- package com.mkyong.exactmethods;
 
public class AllExactMethods {
	
	public static void main(String[] args){
		int x = 10000;
		int y = 10000;
		Object z;
		
		z = Math.addExact(x, y);
		System.out.println("addExact: " + x + " + " + y + " = " + z);
		z = Math.subtractExact(x, y);
		System.out.println("subtractExact: " + x + " - " + y + " = " + z);
		z = Math.multiplyExact(x, y);
		System.out.println("multiplyExact: " + x + " * " + y + " = " + z);
		z = Math.incrementExact(x);
		System.out.println("incrementExact: " + x + " + 1 = " + z);
		z = Math.decrementExact(y);
		System.out.println("decrementExact: " + y + " - 1 = " + z);
		z = Math.negateExact(x);
		System.out.println("negateExact: " + x + " * -1 = " + z);
	}
}
Output- addExact: 10000 + 10000 = 20000
subtractExact: 10000 - 10000 = 0
multiplyExact: 10000 * 10000 = 100000000
incrementExact: 10000 + 1 = 10001
decrementExact: 10000 - 1 = 9999
negateExact: 10000 * -1 = -10000

Q-What is widening type casting and what is narrowing type casting.    

Ans- Widening Casting

This type of casting takes place when two data types are automatically converted. It is also known as Implicit Conversion. This happens when the two data types are compatible and also when we assign the value of a smaller data type to a larger data type.
For Example, The numeric data types are compatible with each other but no automatic conversion is supported from numeric type to char or boolean. Also, char and boolean are not compatible with each other. 

Narrowing Casting

In this case, if you want to assign a value of larger data type to a smaller data type, you can perform Explicit type casting or narrowing. This is useful for incompatible data types where automatic conversion cannot be done.

Q-What are the four rules that are applied in binary operations on numbers.

Ans- If either of the operands is of type double, the other one will be converted to a 
double.
 Otherwise, if either of the operands is of type float, the other one will be 
converted to a float.
 Otherwise, if either of the operands is of type long, the other one will be 
converted to a long.
 Otherwise, both operands will be converted to an int.

Q-Write a program that proves that the four rules are followed in binary operations on numbers.

Ans- public class Operators {
	
	    public static void main(String[] args)
	    {
	 
	        int a = 5;
	        int b = -10;
	 
	        // left shift operator
	       
	        System.out.println("a<<2 = " + (a << 2));
	 
	        // right shift operator
	        
	        System.out.println("b>>2 = " + (b >> 2));
	 
	        // unsigned right shift operator
	        System.out.println("b>>>2 = " + (b >>> 2));
	    }
	}

Q-When we use BigInteger and BigDecimal,does mathematical operators such as+ and* work on these?

Ans- it does not overload the arithmatic (+,-,/,*) or logical (<,> etc) operators.nstead, we use the corresponding methods -add,substract,multiply,divide and compareto.


Q-Write a program that uses BigInteger and BigDecimal.

Ans- package com.divergent.core_java;

import java.io.IOException;
import java.math.BigDecimal;

 public class BigDecimalExample {
 
    public static void main(String args[]) throws IOException {
      
      //floating point calculation
      double amount1 = 2.15;
      double amount2 = 1.10;
      System.out.println("difference between 2.15 and 1.0 using double is: " + (amount1 - amount2));
    
      //Use BigDecimal for financial calculation
      BigDecimal amount3 = new BigDecimal("2.15");
      BigDecimal amount4 = new BigDecimal("1.10") ;
      System.out.println("difference between 2.15 and 1.0 using BigDecimal is: " + (amount3.subtract(amount4)));      
    }     
}





Q-What are the different ways of declaring an array in Java.

Ans- a java array variable is declared just like you would declare a variable of the desired type,except you add[] after type.
        //simply declare
        int[] intarray;
  
     few more ways:
       string[]   stringarray;
       myclass[]  myclassarray;

       //they all are valid.

Q-Why we say that multidimensional arrays in Java are "arrays of arrays".

Ans- java array is an object which contain elements of a similar data type . additionally, the elements of an array are stored  in a contiguous memory location.it is a data structure where we stored similar elements.we can stored only a fixed set of elements in array.

Q-What do you mean by "ragged" arrays?

Ans-In Java, "ragged array" is an “array of array”. This means one can create an array in such a way that each element of the array is a reference to another array of same type. For example, a two-dimensional array of 3 x 2 refers to three rows and two columns (i.e. each row has two columns). We call this as “rectangular array”. In the case of a ragged array, the number of columns may not be fixed.