package divergent1;
import java.io.*;
import java.util.Scanner;

public class CountingVC {
          
                                                // Function to count number of vowels, consonant,
	static void countCharacterType(String str)
		{
		                                      	// Declare the variable vowels, consonant;
			int vowels = 0, consonant = 0;
		
		                               	// str.length() function to count number of character in given string.
			for (int i = 0; i < str.length(); i++) {
				
				char am = str.charAt(i);
		
				if ( (am >= 'a' && am <= 'z') ||    // for lower case
					(am >= 'A' && am <= 'Z') ) {	// for handle upper case letters
					am = Character.toLowerCase(am);;
		
					if (am == 'a' ||am == 'e' || am == 'i' ||
						am == 'o' || am == 'u')
						vowels++;
					else
						consonant++;
				}
				
			}
			
			System.out.println("Vowels: " + vowels);
			System.out.println("Consonant: " + consonant);
			
		}
		
		// Driver function.
		static public void main (String[] args)
		{
			System.out.println(" Enter your sentance:");
			try(Scanner scan = new Scanner(System.in)){
			String str = scan.nextLine();
			countCharacterType(str);
		}
	
		}
}
