package divergent1;

public class Modulo {
	// Java code for Compute modulus division by
	// a power-of-2-number
		
		// This function will return n % d.
		// d must be one of: 1, 2, 4, 8, 16, 32,
		static int getModulo(int n, int d)
		{
			return ( n & (d-1) );
		}	
		
		// Driver Code
		public static void main(String[] args)
		{
			int n = 82;
			
			/*d must be a power of 2*/
			int d = 16;
			
			System.out.println(n+" moduo " + d +
						" is " + getModulo(n, d));
		}
	}

