package divergent1;

import java.util.Scanner;

public class RemoveSpace { 
	    public static void main(String[] args) {  
	    	System.out.println(" Enter your sentance:");
			try(Scanner scan = new Scanner(System.in)){
			String str = scan.nextLine();


	        String noSpaceStr = str.replaceAll("\\s", ""); // using built in method  
	        System.out.println(noSpaceStr);  
}
}
}