package divergent1;

	import java.util.Arrays;
	import java.util.List;

	public class Usingdelimetre
	{
	    public static void main(String[] args)
	    {
	        List<String> alphabets = Arrays.asList("divergent", "software", "lab", "pvt");
	        String delimiter = "";
	 
	        String result = String.join(delimiter, alphabets);
	 
	        System.out.println(result);
	    }
	}

