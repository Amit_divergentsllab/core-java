package divergent1;

//public class MaxMinExample {
	import java.util.Scanner; 
	import java.util.concurrent.Semaphore;
	import java.util.concurrent.locks.Condition;
	import java.util.concurrent.locks.Lock; 
	import java.util.concurrent.locks.ReentrantLock; 
	/** * * Java program to calculate Maximum and minimum of two numbers * entered by user in console. 
	 * * * @author amit chauhan */
	public class MaxMinExample {
		public static void main(String args[])
				throws InterruptedException { 
			Scanner scnr = new Scanner(System.in); // Calculating Maximum two numbers in Java
			System.out.println("Please enter two numbers to find maximum of two");
			int a = scnr.nextInt(); 
			int b = scnr.nextInt(); 
			if (a > b) { System.out.printf("Between %d and %d, maximum is %d %n", a, b, a); 
			}
			else 
			{ 
				System.out.printf("Between %d and %d, maximum number is %d %n", a, b, b); 
			}
			int max = Math.max(a, b); 
			System.out.printf("Maximum value of %d and %d using Math.max() is %d %n", a, b, max); // Calculating Minimum between two numbers in Java 
			System.out.println("Please enter two numbers to find minimum of two");
			int x = scnr.nextInt();
			int y = scnr.nextInt();
			if (x < y) { System.out.printf("Between %d and %d, Minimum Number is %d %n", x, y, x); 
			}
			else
			{ System.out.printf("Between %d and %d, Minimum is %d %n", x, y, y);
			}
			int min = Math.min(x, y); 
			System.out.printf("Maximum value of %d and %d using Math.min() is %d %n", x, y, min);
			}
		
		
	}
	
