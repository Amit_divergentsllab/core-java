package divergent1;
import java.io.*;
import java.util.Scanner;

public class Countoccurance {
	    // Method that return count of the given
	    // character in the string
	    public static int count(String str, char c)
	    {
	        int res = 0;
	 
	        for (int i=0; i<str.length(); i++)
	        {
	            // checking character in string
	            if (str.charAt(i) == c)
	            res++;
	        }
	        return res;
	    }
	     
	    // Driver method
	    public static void main(String args[]) {
	    	System.out.println(" Enter your sentance:");
			try(Scanner scan = new Scanner(System.in)){
			String str = scan.nextLine();
		         char c = 'a';

		        System.out.println(count(str, c));
}

 }}